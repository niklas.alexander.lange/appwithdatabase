package display;

import layout.CustomLayoutData;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;

public class UpdateShell {

    final Shell mainShell;

    public UpdateShell(Shell mainShell){

        this.mainShell = mainShell;
        Shell UpdateShell = new Shell(this.mainShell);
        UpdateShell.setText("Updates");
        UpdateShell.setSize(400, 300);

        GridLayout UpdateShellLay = new GridLayout();
        UpdateShellLay.numColumns = 1;
        UpdateShell.setLayout(UpdateShellLay);

        Group developerGroup = new Group(UpdateShell, SWT.NONE);
        developerGroup.setLayoutData(new CustomLayoutData().fillCompletely());
        developerGroup.setText("Ihre Software befindet sich auf dem aktuellen Stand!");
        GridLayout developerLay = new GridLayout();
        developerLay.numColumns = 1;
        developerGroup.setLayout(developerLay);

        UpdateShell.pack();
        UpdateShell.open();
    }
}
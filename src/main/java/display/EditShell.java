package display;

import database.entities.Niederlassung;
import database.entities.Raum;
import layout.CustomLayoutData;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Shell;
import widgets.Booking;
import widgets.BranchCreationEdit;
import widgets.RoomCreationEditDelete;

public class EditShell {

    final String mode;
    final Shell parent;
    final Object toEdit;
    final Object list;
    final Display display;

    public EditShell(Shell parent, String mode, Object toEdit, Object list) {

        this.mode = mode;
        this.parent = parent;
        this.toEdit = toEdit;
        this.display = parent.getDisplay();
        this.list = list;

        Rectangle guiArea = parent.getBounds();

        Shell editShell = new Shell(this.parent);
        editShell.setSize(guiArea.width, guiArea.height / 2);
        editShell.setMinimumSize(1000,180);
        editShell.setLocation(guiArea.x, guiArea.y + (guiArea.height / 16));
        GridLayout editShellLay = new GridLayout();
        editShellLay.numColumns = 1;
        editShell.setLayout(editShellLay);
        editShell.setLayoutData(new CustomLayoutData().fillHorizontal());

        Group group = new Group(editShell, SWT.NONE);
        group.setText("Bearbeiten");
        group.setLayoutData(new CustomLayoutData().fillHorizontal());
        GridLayout layout = new GridLayout(2, false);
        layout.marginWidth = 0;
        layout.marginHeight = 0;
        group.setLayout(layout);

        if (mode == "room"){
            editShell.setText("Raum bearbeiten");
            editShell.setMinimumSize(1000,380);
            RoomCreationEditDelete edit = new RoomCreationEditDelete(group, "edit", (Raum) toEdit, null);
            edit.addContent(true);
            edit.addButtons();
        }else if (mode == "branch"){
            editShell.setText("Niederlassung bearbeiten");
            editShell.setMinimumSize(1000,220);
            BranchCreationEdit edit = new BranchCreationEdit(group, "edit", (Niederlassung) toEdit);
            edit.addContent();
            edit.addButtons();
        } else {
            if (((Raum) toEdit).isVerfuegbarkeit()) {
                group.setText("Buchen");
                editShell.setText("Raum Buchen");
                Booking booking = new Booking(group, (Raum) toEdit);
                booking.addContent();
            } else {
                new ErrorBox(parent.getShell(), "Hinweis", "Dieser Raum ist zur Zeit nicht verfügbar.");
                return;
            }
        }

        editShell.pack();
        editShell.open();
    }
}

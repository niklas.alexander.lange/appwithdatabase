package display;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;

public class ErrorBox {

    public ErrorBox(Shell parent, String title, String message) {
        MessageBox dialog =
                new MessageBox(parent, SWT.ICON_INFORMATION | SWT.OK);
        dialog.setText(title);
        dialog.setMessage(message);
        dialog.open();
    }

}

package display;

import layout.CustomLayoutData;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.TabFolder;
import tabs.TabFolderCreator;

/**
 * Main class! Creates the Display and one "Main" Shell
 */
public class GUIDisplay {

    public static void main(String args[]) {
        Display display = new Display();
        final Shell shell = new Shell(display);
        shell.setText("Raumbuchungssoftware");
        shell.setImage(new Image(display, "src/main/resources/Icon.png"));
        shell.setMinimumSize(1000, 600);
        Rectangle clientArea = shell.getClientArea();

        /**
         * Menu
         */
        shell.setMenuBar(new MenuCreator(shell).createMenuBar());

        TabFolderCreator tabFolderCreator = new TabFolderCreator(shell, clientArea);
        TabFolder tabs = tabFolderCreator.createMainTabs();

        GridLayout shellLay = new GridLayout();
        shellLay.numColumns = 1;
        shell.setLayout(shellLay);
        tabs.setLayoutData(new CustomLayoutData().fillCompletely());

        /**
         * Necessary SWT Stuff
         */
        shell.open();
        while (!shell.isDisposed()) {
            if (!display.readAndDispatch()) display.sleep();
        }
        display.dispose();
    }
}
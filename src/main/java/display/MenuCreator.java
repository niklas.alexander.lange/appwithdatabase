package display;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.swt.widgets.Shell;

public class MenuCreator {

    final Shell mainShell;

    public MenuCreator(Shell mainShell) {
        this.mainShell = mainShell;
    }

    public Menu createMenuBar(){

        Menu menuBar = new Menu(this.mainShell, SWT.BAR);

        MenuItem fileItem = new MenuItem(menuBar, SWT.CASCADE);
        fileItem.setText("&Datei");
        Menu subFileMenu = new Menu(this.mainShell, SWT.DROP_DOWN);
        fileItem.setMenu(subFileMenu);
        MenuItem selectItem = new MenuItem (subFileMenu, SWT.PUSH);
        selectItem.addListener (SWT.Selection, e -> this.mainShell.dispose());
        selectItem.setText ("&Beenden");

        MenuItem helpItem = new MenuItem(menuBar, SWT.CASCADE);
        helpItem.setText("&Hilfe");
        Menu subHelpMenu = new Menu(this.mainShell, SWT.DROP_DOWN);
        helpItem.setMenu(subHelpMenu);
        MenuItem UpdateItem = new MenuItem (subHelpMenu, SWT.PUSH);
        UpdateItem.addListener(SWT.Selection, e -> new UpdateShell(this.mainShell));
        UpdateItem.setText("&Updates");
        MenuItem aboutItem = new MenuItem (subHelpMenu, SWT.PUSH);
        aboutItem.addListener(SWT.Selection, e -> new AboutShell(this.mainShell));
        aboutItem.setText("&Über");

        return menuBar;
    }
}

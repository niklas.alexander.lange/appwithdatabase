package display;

import layout.CustomLayoutData;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;

public class AboutShell {

    final Shell mainShell;

    public AboutShell(Shell mainShell){

        this.mainShell = mainShell;
        Shell aboutShell = new Shell(this.mainShell);
        aboutShell.setText("Über");
        aboutShell.setSize(400, 300);

        GridLayout aboutShellLay = new GridLayout();
        aboutShellLay.numColumns = 1;
        aboutShell.setLayout(aboutShellLay);

        Group developerGroup = new Group(aboutShell, SWT.NONE);
        developerGroup.setLayoutData(new CustomLayoutData().fillCompletely());
        developerGroup.setText("Entwickelt von:");
        GridLayout developerLay = new GridLayout();
        developerLay.numColumns = 1;
        developerGroup.setLayout(developerLay);
        Label copyright = new Label(developerGroup, SWT.NONE);
        copyright.setLayoutData(new CustomLayoutData().fillCompletely());
        copyright.setText("© 2020, Raumbuchungssoftware");
        Label tobi = new Label(developerGroup, SWT.NONE);
        tobi.setLayoutData(new CustomLayoutData().fillCompletely());
        tobi.setText("Tobias Henn \t \t tobi@example.com");
        Label jan = new Label(developerGroup, SWT.NONE);
        jan.setText("Jan Ruprecht \t \t jan@example.com");
        jan.setLayoutData(new CustomLayoutData().fillCompletely());
        Label tim = new Label(developerGroup, SWT.NONE);
        tim.setLayoutData(new CustomLayoutData().fillCompletely());
        tim.setText("Tim Endert \t \t tim@example.com");
        Label mahbob = new Label(developerGroup, SWT.NONE);
        mahbob.setLayoutData(new CustomLayoutData().fillCompletely());
        mahbob.setText("Mhbob Alhoussen \t mhbob@example.com");
        Label boss = new Label(developerGroup, SWT.NONE);
        boss.setLayoutData(new CustomLayoutData().fillCompletely());
        boss.setText("Niklas Lange \t \t niklas@example.com");

        aboutShell.pack();
        aboutShell.open();
    }
}

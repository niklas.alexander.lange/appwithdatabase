package widgets;

import database.DatabaseController;
import database.entities.Buchungstermin;
import database.entities.Raum;
import display.ErrorBox;
import layout.CustomLayoutData;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.*;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.Calendar;
import java.util.Date;

public class Booking {

    final Group parent;
    final Raum raum;
    final DatabaseController database;

    Text roomInput;
    Text employeeInput;
    DateTime dateOfBookingBeginningInputDate;
    DateTime dateOfBookingBeginningInputTime;
    DateTime dateOfBookingEndInputDate;
    DateTime dateOfBookingEndInputTime;

    public Booking(Group parent, Raum raum) {
        this.parent = parent;
        this.raum = raum;
        this.database = new DatabaseController();
        database.start();
    }
    
    public void addContent(){
        
        Composite Com = new Composite(this.parent, SWT.NONE);
        Com.setLayoutData(new CustomLayoutData().fillCompletely());
        GridLayout compositeLayout = new GridLayout(3, false);
        Com.setLayout(compositeLayout);

        fillCom(Com);
        addButtons();
    }

    private void fillCom(Composite com) {

        Label roomLabel = new Label(com, SWT.NONE);
        roomLabel.setText("Raum*: ");
        roomInput = new Text(com, SWT.BORDER);
        roomInput.setEditable(false);
        roomInput.setText(raum.getName() + " " + raum.getRaumnummer());
        roomInput.setLayoutData(new CustomLayoutData().fillHorizontalSpan(2));

        Label employeeLabel = new Label(com, SWT.NONE);
        employeeLabel.setText("Mitarbeiter*: ");
        employeeInput = new Text(com, SWT.BORDER);
        employeeInput.setLayoutData(new CustomLayoutData().fillHorizontalSpan(2));

        Label dateOfBookingBeginningLabel = new Label(com, SWT.NONE);
        dateOfBookingBeginningLabel.setText("Buchungs Startdatum*: ");
        dateOfBookingBeginningInputDate = new DateTime(com, SWT.DATE);
        dateOfBookingBeginningInputDate.setLayoutData(new CustomLayoutData().fillHorizontal());
        dateOfBookingBeginningInputTime = new DateTime(com, SWT.TIME);
        dateOfBookingBeginningInputTime.setLayoutData(new CustomLayoutData().fillHorizontal());

        Label dateOfBookingEndLabel = new Label(com, SWT.NONE);
        dateOfBookingEndLabel.setText("Buchungs Enddatum*: ");
        dateOfBookingEndInputDate = new DateTime(com, SWT.DATE);
        dateOfBookingEndInputDate.setLayoutData(new CustomLayoutData().fillHorizontal());
        dateOfBookingEndInputTime = new DateTime(com, SWT.TIME);
        dateOfBookingEndInputTime.setLayoutData(new CustomLayoutData().fillHorizontal());
    }

    private void addButtons() {
        Composite buttonComposite = new Composite(this.parent, SWT.NONE);
        buttonComposite.setLayoutData(new CustomLayoutData().fillHorizontalWithButtonHeight(3));
        GridLayout buttonComLayout = new GridLayout(3, false);
        buttonComLayout.marginHeight = 0;
        buttonComLayout.marginWidth = 0;
        buttonComposite.setLayout(buttonComLayout);

        Composite buttonFiller =new Composite(buttonComposite, SWT.NONE);
        buttonFiller.setLayoutData(new CustomLayoutData().horizontalFiller());
        Button clearFieldButton = new Button(buttonComposite, SWT.NONE);
        clearFieldButton.setText("Felder leeren");
        clearFieldButton.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent e) {
                clearFields();
            }
        });
        clearFieldButton.setLayoutData(new CustomLayoutData().alignButtonRight());

        Button bookRoomButton = new Button(buttonComposite, SWT.NONE);
        bookRoomButton.setText("Ok");
        bookRoomButton.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent e) {
                bookRoom();
            }
        });
        bookRoomButton.setLayoutData(new CustomLayoutData().alignButtonRight());
    }

    private void bookRoom() {
        if (employeeInput.getText().replaceAll("\\s","").length() > 0) {
            Calendar beginning = Calendar.getInstance();
            beginning.set(dateOfBookingBeginningInputDate.getYear(), dateOfBookingBeginningInputDate.getMonth(), dateOfBookingBeginningInputDate.getDay(), dateOfBookingBeginningInputTime.getHours(), dateOfBookingBeginningInputTime.getMinutes(), dateOfBookingBeginningInputTime.getSeconds());
            Calendar end = Calendar.getInstance();
            end.set(dateOfBookingEndInputDate.getYear(), dateOfBookingEndInputDate.getMonth(), dateOfBookingEndInputDate.getDay(), dateOfBookingEndInputTime.getHours(), dateOfBookingEndInputTime.getMinutes(), dateOfBookingEndInputTime.getSeconds());
            if (end.compareTo(beginning) == 1) {
                if (database.canRoomBeBooked(raum, new Timestamp(beginning.getTimeInMillis()), new Timestamp(end.getTimeInMillis()))) {
                    Buchungstermin booking = new Buchungstermin(employeeInput.getText(),
                            new Timestamp(beginning.getTimeInMillis()),
                            new Timestamp(end.getTimeInMillis()),
                            new Timestamp(System.currentTimeMillis()), raum);
                    database.createBooking(booking);
                    parent.getShell().close();
                } else {
                    new ErrorBox(parent.getShell(), "Hinweis", "Der Raum ist zu diesem Zeitpunkt leider besetzt.");
                }
            } else {
                new ErrorBox(parent.getShell(), "Hinweis", "Das Enddatum muss nach dem Startdatum liegen.");
            }
        } else {
            new ErrorBox(parent.getShell(), "Hinweis", "Bitte füllen Sie alle benötigten Felder aus.");
        }
    }

    private void clearFields() {
        employeeInput.setText("");
        dateOfBookingBeginningInputDate.setDate(LocalDateTime.now().getYear(), LocalDateTime.now().getMonthValue(), LocalDateTime.now().getDayOfMonth());
        dateOfBookingEndInputDate.setDate(LocalDateTime.now().getYear(), LocalDateTime.now().getMonthValue(), LocalDateTime.now().getDayOfMonth());
    }
}

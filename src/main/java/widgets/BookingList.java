package widgets;

import database.DatabaseController;
import database.entities.Buchungstermin;
import database.entities.Niederlassung;
import database.entities.Raum;
import display.ErrorBox;
import layout.CustomLayoutData;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ControlAdapter;
import org.eclipse.swt.events.ControlEvent;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.*;

import java.util.List;

public class BookingList {

    final Group parent;
    final DatabaseController database;
    Raum latestRoom;

    Button updateButton;

    Table table;

    public BookingList(Group parent) {
        this.parent = parent;
        this.database = new DatabaseController();
        database.start();

        addContent();
        addButtons();
    }

    public void addContent(){

        table = new Table(parent, SWT.V_SCROLL);
        TableColumn roomName = new TableColumn(table, SWT.NONE);
        roomName.setText("Raum Name");
        TableColumn roomNumber = new TableColumn(table, SWT.NONE);
        roomNumber.setText("Raum Nummmer");
        TableColumn employee = new TableColumn(table, SWT.NONE);
        employee.setText("Mitarbeiter");
        TableColumn dateOfCreation = new TableColumn(table, SWT.NONE);
        dateOfCreation.setText("Erstelllungsdatum");
        TableColumn dateBeginning = new TableColumn(table, SWT.NONE);
        dateBeginning.setText("Startdatum");
        TableColumn dateEnd = new TableColumn(table, SWT.NONE);
        dateEnd.setText("Enddatum");

        parent.addControlListener(new ControlAdapter() {
            public void controlResized(ControlEvent e) {
                Rectangle area = parent.getClientArea();
                Point preferredSize = table.computeSize(SWT.DEFAULT, SWT.DEFAULT);
                int width = area.width - 2*table.getBorderWidth();
                if (preferredSize.y > area.height + table.getHeaderHeight()) {
                    // Subtract the scrollbar width from the total column width
                    // if a vertical scrollbar will be required
                    Point vBarSize = table.getVerticalBar().getSize();
                    width -= vBarSize.x;
                }
                Point oldSize = table.getSize();
                if (oldSize.x > area.width) {
                    // table is getting smaller so make the columns
                    // smaller first and then resize the table to
                    // match the client area width
                    roomName.setWidth(width/6);
                    roomNumber.setWidth(width/6);
                    employee.setWidth(width/6);
                    dateOfCreation.setWidth(width/6);
                    dateBeginning.setWidth(width/6);
                    dateEnd.setWidth(width/6);
                    table.setSize(area.width, area.height);
                } else {
                    // table is getting bigger so make the table
                    // bigger first and then make the columns wider
                    // to match the client area width
                    table.setSize(area.width, area.height);
                    roomName.setWidth(width/6);
                    roomNumber.setWidth(width/6);;
                    employee.setWidth(width/6);;
                    dateOfCreation.setWidth(width/6);;
                    dateBeginning.setWidth(width/6);;
                    dateEnd.setWidth(width/6);
                }
            }
        });

        table.setHeaderVisible(true);
        table.setLayoutData(new CustomLayoutData().fillCompletely());
    }

    public void fillList(Raum raum) {

        latestRoom = raum;

        updateButton.setEnabled(true);

        table.removeAll();

        List<Buchungstermin> bookings = database.getBookings(raum);
        for (int i = 0; i < bookings.size(); i++){
            Buchungstermin booking = bookings.get(i);
            TableItem item = new TableItem(table, SWT.NONE);
            item.setData(booking);
            item.setText(0, raum.getName());
            item.setText(1, raum.getRaumnummer());
            item.setText(2, booking.getMitarbeiter());
            item.setText(3, booking.getErstellung().toString());
            item.setText(4, booking.getStart().toString());
            item.setText(5, booking.getEnde().toString());
        }
    }

    public void addButtons() {

        Composite buttonComposite = new Composite(this.parent, SWT.NONE);
        buttonComposite.setLayoutData(new CustomLayoutData().fillHorizontalWithButtonHeight(3));
        GridLayout buttonComLayout = new GridLayout(3, false);
        buttonComLayout.marginHeight = 0;
        buttonComLayout.marginWidth = 0;
        buttonComposite.setLayout(buttonComLayout);

        Composite buttonFiller =new Composite(buttonComposite, SWT.NONE);
        buttonFiller.setLayoutData(new CustomLayoutData().horizontalFiller());
        updateButton = new Button(buttonComposite, SWT.NONE);
        updateButton.setEnabled(false);
        updateButton.setText("Aktualisieren");
        updateButton.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent e) {
                fillList(latestRoom);
            }
        });
        updateButton.setLayoutData(new CustomLayoutData().alignButtonRight());
        Button deleteButton = new Button(buttonComposite, SWT.NONE);
        deleteButton.setText("Löschen");
        deleteButton.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent e) {
                try {
                    deleteBooking();
                    fillList(latestRoom);
                } catch (ArrayIndexOutOfBoundsException exception) {
                    new ErrorBox(parent.getShell(),
                            "Hinweis",
                            "Bitte klicken Sie zum Löschen zunächst auf den Namen des Eintrages.");
                }
            }
        });
        deleteButton.setLayoutData(new CustomLayoutData().alignButtonRight());
    }

    private void deleteBooking() {

        database.deleteBooking((Buchungstermin) table.getSelection()[0].getData());
    }
}

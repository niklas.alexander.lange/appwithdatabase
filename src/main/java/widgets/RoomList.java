package widgets;

import database.DatabaseController;
import database.entities.Ausstattung;
import database.entities.Niederlassung;
import database.entities.Raum;
import display.EditShell;
import display.ErrorBox;
import layout.CustomLayoutData;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ControlAdapter;
import org.eclipse.swt.events.ControlEvent;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.*;

import java.util.List;

public class RoomList {

    final Group parent;
    final DatabaseController database;

    Table table;

    BookingList bookingList;

    public RoomList(Group parent) {
        this.parent = parent;
        this.database = new DatabaseController();
        database.start();
    }

    public void addContent() {

        table = new Table(parent, SWT.V_SCROLL | SWT.HIDE_SELECTION);
        TableColumn roomName = new TableColumn(table, SWT.NONE);
        roomName.setText("Name");
        TableColumn roomNumber = new TableColumn(table, SWT.NONE);
        roomNumber.setText("Nummer");
        TableColumn capacity = new TableColumn(table, SWT.NONE);
        capacity.setText("Kapazität");
        TableColumn availability = new TableColumn(table, SWT.NONE);
        availability.setText("Verfügbarkeit");
        TableColumn departmentName = new TableColumn(table, SWT.NONE);
        departmentName.setText("Niederlassung");

        TableColumn computer = new TableColumn(table, SWT.NONE);
        computer.setText("Computer");
        TableColumn phones = new TableColumn(table, SWT.NONE);
        phones.setText("Telefone");
        TableColumn seats = new TableColumn(table, SWT.NONE);
        seats.setText("Stühle");
        TableColumn tables = new TableColumn(table, SWT.NONE);
        tables.setText("Tische");
        TableColumn beaner = new TableColumn(table, SWT.NONE);
        beaner.setText("Beamer");
        TableColumn tvs = new TableColumn(table, SWT.NONE);
        tvs.setText("Fernseher");

        parent.addControlListener(new ControlAdapter() {
            public void controlResized(ControlEvent e) {
                Rectangle area = parent.getClientArea();
                Point preferredSize = table.computeSize(SWT.DEFAULT, SWT.DEFAULT);
                int width = area.width - 2 * table.getBorderWidth();
                if (preferredSize.y > area.height + table.getHeaderHeight()) {
                    // Subtract the scrollbar width from the total column width
                    // if a vertical scrollbar will be required
                    Point vBarSize = table.getVerticalBar().getSize();
                    width -= vBarSize.x;
                }
                Point oldSize = table.getSize();
                if (oldSize.x > area.width) {
                    // table is getting smaller so make the columns
                    // smaller first and then resize the table to
                    // match the client area width
                    roomName.setWidth(width / 11);
                    roomNumber.setWidth(width / 11);
                    capacity.setWidth(width / 11);
                    availability.setWidth(width / 11);
                    departmentName.setWidth(width / 11);
                    computer.setWidth(width / 11);
                    phones.setWidth(width / 11);
                    seats.setWidth(width / 11);
                    tables.setWidth(width / 11);
                    beaner.setWidth(width / 11);
                    tvs.setWidth(width / 11);
                    table.setSize(area.width, area.height);
                } else {
                    // table is getting bigger so make the table
                    // bigger first and then make the columns wider
                    // to match the client area width
                    table.setSize(area.width, area.height);
                    roomName.setWidth(width / 11);
                    roomNumber.setWidth(width / 11);
                    capacity.setWidth(width / 11);
                    availability.setWidth(width / 11);
                    departmentName.setWidth(width / 11);
                    computer.setWidth(width / 11);
                    phones.setWidth(width / 11);
                    seats.setWidth(width / 11);
                    tables.setWidth(width / 11);
                    beaner.setWidth(width / 11);
                    tvs.setWidth(width / 11);
                }
            }
        });

        fillList();

        table.setHeaderVisible(true);
        table.setLayoutData(new CustomLayoutData().fillCompletely());
    }

    public void fillList() {
        table.removeAll();

        List<Niederlassung> branches = database.getAllBranches();
        List<Raum> rooms = database.getAllRooms();

        for (int i = 0; i < rooms.size(); i++) {
            Raum room = rooms.get(i);
            Niederlassung branch = null;
            for (int x = 0; x < branches.size(); x++) {
                long roomId = room.getNiederlassung().getId();
                Long branchId = branches.get(x).getId();
                if (roomId == branchId) {
                    branch = branches.get(x);
                }
            }
            Ausstattung assets = database.getAssets(room);
            TableItem item = new TableItem(table, SWT.NONE);
            item.setData(room);
            item.setText(0, room.getName());
            item.setText(1, room.getRaumnummer());
            item.setText(2, String.valueOf(room.getKapazitaet()));
            if (room.isVerfuegbarkeit()) {
                item.setText(3, "Verfügbar");
            } else {
                item.setText(3, "Nicht Verfügbar");
            }
            assert branch != null;
            item.setText(4, branch.getName());
            item.setText(5, branch.getStrasse());
            item.setText(6, branch.getHausnummer());
            item.setText(7, branch.getPlz());
            item.setText(8, branch.getOrt());
            item.setText(5, String.valueOf(assets.getComputer()));
            item.setText(6, String.valueOf(assets.getTelefone()));
            item.setText(7, String.valueOf(assets.getStuehle()));
            item.setText(8, String.valueOf(assets.getTische()));
            item.setText(9, String.valueOf(assets.getBeamer()));
            item.setText(10, String.valueOf(assets.getFernseher()));
        }
    }

    public void fillListWithFilter(String roomName,int capacity,String roomNumber,boolean available,String departmentName,String departmentStreet,String departmentStreetNumber,String departmentPlz,String departmentLocation,int computer,int phones,int seats,int tables,int beamer,int tvs) {
        table.removeAll();

        List<Niederlassung> branches = database.getAllBranches();
        List<Raum> rooms = database.getFilteredRooms(roomName, capacity, roomNumber, available, departmentName, departmentStreet, departmentStreetNumber, departmentPlz, departmentLocation, computer, phones, seats, tables, beamer, tvs);

        for (int i = 0; i < rooms.size(); i++){
            Raum room = rooms.get(i);
            Niederlassung branch = null;
            for (int x = 0; x < branches.size(); x++){
                long roomId = room.getNiederlassung().getId();
                Long branchId = branches.get(x).getId();
                if (roomId == branchId){
                    branch = branches.get(x);
                }
            }
            Ausstattung assets = database.getAssets(room);
            TableItem item = new TableItem(table, SWT.NONE);
            item.setData(room);
            item.setText(0, room.getName());
            item.setText(1, room.getRaumnummer());
            item.setText(2, String.valueOf(room.getKapazitaet()));
            if (room.isVerfuegbarkeit()){
                item.setText(3, "Verfügbar");
            } else {
                item.setText(3, "Nicht Verfügbar");
            }
            assert branch != null;
            item.setText(4, branch.getName());
            item.setText(5, branch.getStrasse());
            item.setText(6, branch.getHausnummer());
            item.setText(7, branch.getPlz());
            item.setText(8, branch.getOrt());
            item.setText(5, String.valueOf(assets.getComputer()));
            item.setText(6, String.valueOf(assets.getTelefone()));
            item.setText(7, String.valueOf(assets.getStuehle()));
            item.setText(8, String.valueOf(assets.getTische()));
            item.setText(9, String.valueOf(assets.getBeamer()));
            item.setText(10, String.valueOf(assets.getFernseher()));
        }
    }

    public void addEditDeleteButtons() {

        Composite buttonComposite = new Composite(this.parent, SWT.NONE);
        buttonComposite.setLayoutData(new CustomLayoutData().fillHorizontalWithButtonHeight(3));
        GridLayout buttonComLayout = new GridLayout(4, false);
        buttonComLayout.marginHeight = 0;
        buttonComLayout.marginWidth = 0;
        buttonComposite.setLayout(buttonComLayout);

        Composite buttonFiller = new Composite(buttonComposite, SWT.NONE);
        buttonFiller.setLayoutData(new CustomLayoutData().horizontalFiller());
        Button updateButton = new Button(buttonComposite, SWT.NONE);
        updateButton.setText("Aktualisieren");
        updateButton.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent e) {
                fillList();
            }
        });
        updateButton.setLayoutData(new CustomLayoutData().alignButtonRight());
        Button editButton = new Button(buttonComposite, SWT.NONE);
        editButton.setText("Bearbeiten");
        editButton.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent e) {
                try {
                    new EditShell(parent.getShell(), "room", table.getSelection()[0].getData(), this);
                } catch (ArrayIndexOutOfBoundsException exception) {
                    new ErrorBox(parent.getShell(),
                            "Hinweis",
                            "Bitte klicken Sie zur Bearbeitung zunächst auf den Namen des Eintrages.");
                }
            }
        });
        editButton.setLayoutData(new CustomLayoutData().alignButtonRight());
        Button deleteButton = new Button(buttonComposite, SWT.NONE);
        deleteButton.setText("Löschen");
        deleteButton.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent e) {
                try {
                    database.deleteRoom((Raum) table.getSelection()[0].getData());
                    fillList();
                } catch (ArrayIndexOutOfBoundsException exception) {
                    new ErrorBox(parent.getShell(),
                            "Hinweis",
                            "Bitte klicken Sie zum Löschen zunächst auf den Namen des Eintrages.");
                }
            }
        });
        deleteButton.setLayoutData(new CustomLayoutData().alignButtonRight());
    }

    public void addBookingButtons() {

        Composite buttonComposite = new Composite(this.parent, SWT.NONE);
        buttonComposite.setLayoutData(new CustomLayoutData().fillHorizontalWithButtonHeight(2));
        GridLayout buttonComLayout = new GridLayout(2, false);
        buttonComLayout.marginHeight = 0;
        buttonComLayout.marginWidth = 0;
        buttonComposite.setLayout(buttonComLayout);

        Composite buttonFiller = new Composite(buttonComposite, SWT.NONE);
        buttonFiller.setLayoutData(new CustomLayoutData().horizontalFiller());
        Button editButton = new Button(buttonComposite, SWT.NONE);
        editButton.setText("Buchen");
        editButton.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent e) {
                try {
                    new EditShell(parent.getShell(), "booking", table.getSelection()[0].getData(), null);
                } catch (ArrayIndexOutOfBoundsException exception) {
                    new ErrorBox(parent.getShell(),
                            "Hinweis",
                            "Bitte klicken Sie zum Buchen zunächst auf einen Raum.");
                }
            }
        });
        editButton.setLayoutData(new CustomLayoutData().alignButtonRight());
        table.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent e) {
                bookingList.fillList((Raum) table.getSelection()[0].getData());
            }
        });
    }

    public void setBookingList(BookingList list){
        bookingList = list;
    }

}

package widgets;

import database.DatabaseController;
import database.entities.Niederlassung;
import display.ErrorBox;
import layout.CustomLayoutData;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.*;

public class BranchCreationEdit {

    final Group parent;
    final String mode;
    final Niederlassung niederlassung;

    final DatabaseController database;

    BranchList branchList;

    Text nameOfDepartmentInput;
    Text streetNameInput;
    Text streetNumberInput;
    Text plzNumberInput;
    Text locationInput;

    public BranchCreationEdit(Group parent, String mode, Niederlassung niederlassung) {

        this.parent = parent;
        this.mode = mode;
        this.niederlassung = niederlassung;

        this.database = new DatabaseController();
        database.start();
    }

    public void addContent() {

        Composite Com = new Composite(this.parent, SWT.NONE);
        Com.setLayoutData(new CustomLayoutData().fillCompletely());
        GridLayout compositeLayout = new GridLayout(2, false);
        Com.setLayout(compositeLayout);

        fillCom(Com);

        if (mode == "edit"){
             fillInputs();
        }
    }

    private void fillInputs() {
        nameOfDepartmentInput.setText(niederlassung.getName());
        streetNameInput.setText(niederlassung.getStrasse());
        streetNumberInput.setText(niederlassung.getHausnummer());
        plzNumberInput.setText(niederlassung.getPlz());
        locationInput.setText(niederlassung.getOrt());
    }

    private void fillCom(Composite com) {

        Label nameOfDepartmentLabel = new Label(com, SWT.NONE);
        nameOfDepartmentLabel.setText("Name*: ");
        nameOfDepartmentInput = new Text(com, SWT.BORDER);
        nameOfDepartmentInput.setLayoutData(new CustomLayoutData().fillHorizontal());

        Label streetNameLabel = new Label(com, SWT.NONE);
        streetNameLabel.setText("Straße*: ");
        streetNameInput = new Text(com, SWT.BORDER);
        streetNameInput.setLayoutData(new CustomLayoutData().fillHorizontal());

        Label streetNumberLabel = new Label(com, SWT.NONE);
        streetNumberLabel.setText("Straßennummer*: ");
        streetNumberInput = new Text(com, SWT.BORDER);
        streetNumberInput.setLayoutData(new CustomLayoutData().fillHorizontal());

        Label plzNumberLabel = new Label(com, SWT.NONE);
        plzNumberLabel.setText("Postleitzahl*: ");
        plzNumberInput = new Text(com, SWT.BORDER);
        plzNumberInput.setLayoutData(new CustomLayoutData().fillHorizontal());

        Label locationLabel = new Label(com, SWT.NONE);
        locationLabel.setText("Ort*: ");
        locationLabel.setLayoutData(new CustomLayoutData().alignTop());
        locationInput = new Text(com, SWT.BORDER);
        locationInput.setLayoutData(new CustomLayoutData().fillCompletely());

    }

    public void addButtons() {

        Composite buttonComposite = new Composite(this.parent, SWT.NONE);
        buttonComposite.setLayoutData(new CustomLayoutData().fillHorizontalWithButtonHeight(3));
        GridLayout buttonComLayout = new GridLayout(3, false);
        buttonComLayout.marginHeight = 0;
        buttonComLayout.marginWidth = 0;
        buttonComposite.setLayout(buttonComLayout);

        Composite buttonFiller =new Composite(buttonComposite, SWT.NONE);
        buttonFiller.setLayoutData(new CustomLayoutData().horizontalFiller());
        Button clearFieldButton = new Button(buttonComposite, SWT.NONE);
        clearFieldButton.setText("Felder leeren");
        clearFieldButton.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent e) {
                clearFields();
            }
        });
        clearFieldButton.setLayoutData(new CustomLayoutData().alignButtonRight());
        if (mode == "create"){
            Button createRoomButton = new Button(buttonComposite, SWT.NONE);
            createRoomButton.setText("Erstellen");
            createRoomButton.addSelectionListener(new SelectionAdapter() {
                @Override
                public void widgetSelected(SelectionEvent e) {
                    createBranch();
                    branchList.fillList();
                }
            });
            createRoomButton.setLayoutData(new CustomLayoutData().alignButtonRight());
        } else {
            Button editRoomButton = new Button(buttonComposite, SWT.NONE);
            editRoomButton.setText("Ok");
            editRoomButton.addSelectionListener(new SelectionAdapter() {
                @Override
                public void widgetSelected(SelectionEvent e) {
                    editBranch();
                }
            });
            editRoomButton.setLayoutData(new CustomLayoutData().alignButtonRight());
        }
    }

    private void createBranch() {
        if (nameOfDepartmentInput.getText().replaceAll("\\s","").length() > 0 &&
                streetNameInput.getText().replaceAll("\\s","").length() > 0 &&
                streetNumberInput.getText().replaceAll("\\s","").length() > 0 &&
                plzNumberInput.getText().replaceAll("\\s","").length() > 0 &&
                locationInput.getText().replaceAll("\\s","").length() > 0) {
            Niederlassung branch = new Niederlassung(nameOfDepartmentInput.getText(), streetNameInput.getText(), streetNumberInput.getText(), plzNumberInput.getText(), locationInput.getText());
            database.createBranch(branch);
            clearFields();
        } else {
            new ErrorBox(parent.getShell(), "Hinweis", "Bitte füllen Sie alle benötigten Felder aus.");
        }
    }

    private void editBranch() {
        if (nameOfDepartmentInput.getText().replaceAll("\\s","").length() > 0 &&
                streetNameInput.getText().replaceAll("\\s","").length() > 0 &&
                streetNumberInput.getText().replaceAll("\\s","").length() > 0 &&
                plzNumberInput.getText().replaceAll("\\s","").length() > 0 &&
                locationInput.getText().replaceAll("\\s","").length() > 0) {
            Niederlassung branch = new Niederlassung(nameOfDepartmentInput.getText(), streetNameInput.getText(), streetNumberInput.getText(), plzNumberInput.getText(), locationInput.getText());
            branch.setId(niederlassung.getId());
            database.editBranch(branch);
            parent.getShell().close();
        } else {
            new ErrorBox(parent.getShell(), "Hinweis", "Bitte füllen Sie alle benötigten Felder aus.");
        }
    }

    private void clearFields() {
        nameOfDepartmentInput.setText("");
        streetNameInput.setText("");
        streetNumberInput.setText("");
        plzNumberInput.setText("");
        locationInput.setText("");
    }

    public void setBranchList(BranchList list){
        this.branchList = list;
    }
}

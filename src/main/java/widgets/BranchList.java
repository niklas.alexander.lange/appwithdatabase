package widgets;

import database.DatabaseController;
import database.entities.Niederlassung;
import display.EditShell;
import display.ErrorBox;
import layout.CustomLayoutData;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ControlAdapter;
import org.eclipse.swt.events.ControlEvent;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.*;

import java.util.List;

public class BranchList {

    final Group parent;
    final DatabaseController database;

    Table table;

    public BranchList(Group parent) {
        this.parent = parent;
        this.database = new DatabaseController();
        database.start();
    }

    public void addContent() {

        table = new Table(parent, SWT.V_SCROLL);
        TableColumn name = new TableColumn(table, SWT.NONE);
        name.setText("Name");
        TableColumn streetName = new TableColumn(table, SWT.NONE);
        streetName.setText("Straße");
        TableColumn streetNumber = new TableColumn(table, SWT.NONE);
        streetNumber.setText("Straßennummer");
        TableColumn plz = new TableColumn(table, SWT.NONE);
        plz.setText("Postleitzahl");
        TableColumn location = new TableColumn(table, SWT.NONE);
        location.setText("Ort");

        parent.addControlListener(new ControlAdapter() {
            public void controlResized(ControlEvent e) {
                Rectangle area = parent.getClientArea();
                Point preferredSize = table.computeSize(SWT.DEFAULT, SWT.DEFAULT);
                int width = area.width - 2 * table.getBorderWidth();
                if (preferredSize.y > area.height + table.getHeaderHeight()) {
                    // Subtract the scrollbar width from the total column width
                    // if a vertical scrollbar will be required
                    Point vBarSize = table.getVerticalBar().getSize();
                    width -= vBarSize.x;
                }
                Point oldSize = table.getSize();
                if (oldSize.x > area.width) {
                    // table is getting smaller so make the columns
                    // smaller first and then resize the table to
                    // match the client area width
                    name.setWidth(width / 5);
                    streetName.setWidth(width / 5);
                    streetNumber.setWidth(width / 5);
                    plz.setWidth(width / 5);
                    location.setWidth(width / 5);
                    table.setSize(area.width, area.height);
                } else {
                    // table is getting bigger so make the table
                    // bigger first and then make the columns wider
                    // to match the client area width
                    table.setSize(area.width, area.height);
                    name.setWidth(width / 5);
                    streetName.setWidth(width / 5);
                    ;
                    streetNumber.setWidth(width / 5);
                    ;
                    plz.setWidth(width / 5);
                    ;
                    location.setWidth(width / 5);
                    ;
                }
            }
        });

        fillList();

        table.setHeaderVisible(true);
        table.setLayoutData(new CustomLayoutData().fillCompletely());
    }

    public void fillList() {

        table.removeAll();

        List<Niederlassung> branches = database.getAllBranches();
        for (int i = 0; i < branches.size(); i++) {
            Niederlassung branch = branches.get(i);
            TableItem item = new TableItem(table, SWT.NONE);
            item.setData(branch);
            item.setText(0, branch.getName());
            item.setText(1, branch.getStrasse());
            item.setText(2, branch.getHausnummer());
            item.setText(3, branch.getPlz());
            item.setText(4, branch.getOrt());
        }
    }

    public void addButtons() {

        Composite buttonComposite = new Composite(this.parent, SWT.NONE);
        buttonComposite.setLayoutData(new CustomLayoutData().fillHorizontalWithButtonHeight(3));
        GridLayout buttonComLayout = new GridLayout(4, false);
        buttonComLayout.marginHeight = 0;
        buttonComLayout.marginWidth = 0;
        buttonComposite.setLayout(buttonComLayout);

        Composite buttonFiller = new Composite(buttonComposite, SWT.NONE);
        buttonFiller.setLayoutData(new CustomLayoutData().horizontalFiller());
        Button updateButton = new Button(buttonComposite, SWT.NONE);
        updateButton.setText("Aktualisieren");
        updateButton.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent e) {
                fillList();
            }
        });
        updateButton.setLayoutData(new CustomLayoutData().alignButtonRight());
        Button editButton = new Button(buttonComposite, SWT.NONE);
        editButton.setText("Bearbeiten");
        editButton.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent e) {
                try {
                    new EditShell(parent.getShell(), "branch", table.getSelection()[0].getData(), this);
                } catch (Exception exception) {
                    new ErrorBox(parent.getShell(),
                            "Hinweis",
                            "Bitte klicken Sie zur Bearbeitung zunächst auf den Namen des Eintrages.");
                }
            }
        });
        editButton.setLayoutData(new CustomLayoutData().alignButtonRight());
        Button deleteButton = new Button(buttonComposite, SWT.NONE);
        deleteButton.setText("Löschen");
        deleteButton.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent e) {
                try {
                    deleteBranch();
                    fillList();
                } catch (ArrayIndexOutOfBoundsException exception) {
                    new ErrorBox(parent.getShell(),
                            "Hinweis",
                            "Bitte klicken Sie zum Löschen zunächst auf den Namen des Eintrages.");
                }
            }
        });
        deleteButton.setLayoutData(new CustomLayoutData().alignButtonRight());
    }

    private void deleteBranch() {

        database.deleteBranch((Niederlassung) table.getSelection()[0].getData());
    }
}

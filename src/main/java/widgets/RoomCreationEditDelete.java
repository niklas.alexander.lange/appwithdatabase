package widgets;

import database.DatabaseController;
import database.entities.Ausstattung;
import database.entities.Niederlassung;
import database.entities.Raum;
import display.ErrorBox;
import layout.CustomLayoutData;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.*;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.*;

import java.time.LocalDateTime;
import java.util.List;

public class RoomCreationEditDelete {

    final Group parent;
    final String mode;
    final Raum raum;
    final DatabaseController database;
    RoomList roomList;

    Text numberInput;
    Text nameInput;
    Spinner capacityInput;
    Button availableButton;
    Text descriptionInput;

    Combo nameOfDepartmentInput;
    Text streetWithNumberInput;
    Text plzNumberInput;
    Text locationInput;
    Spinner computerInput;
    Spinner beamerInput;
    Spinner phoneInput;
    Spinner seatInput;
    Spinner tableInput;
    Spinner tvInput;

    Text employeeInput;
    DateTime dateOfBookingBeginningInput;
    DateTime dateOfBookingBeginningInputTime;
    DateTime dateOfBookingEndInput;
    DateTime dateOfBookingEndInputTime;

    public RoomCreationEditDelete(Group parent, String mode, Raum raum, RoomList roomList) {

        this.parent = parent;
        this.mode = mode;
        this.raum = raum;
        this.roomList = roomList;

        this.database = new DatabaseController();
        database.start();
    }

    public void addContent(boolean useRequiredMark) {

        Composite leftCom = new Composite(this.parent, SWT.NONE);
        Composite rightCom = new Composite(this.parent, SWT.NONE);
        leftCom.setLayoutData(new CustomLayoutData().fillCompletely());
        rightCom.setLayoutData(new CustomLayoutData().fillCompletely());
        GridLayout compositeLayout = new GridLayout(2, false);
        leftCom.setLayout(compositeLayout);
        rightCom.setLayout(compositeLayout);

        fillLeftCom(leftCom, useRequiredMark);
        fillRightCom(rightCom, useRequiredMark);

        if (mode == "search"){
            Composite extraComForSearch = new Composite(this.parent, SWT.NONE);
            extraComForSearch.setLayoutData(new CustomLayoutData().fillCompletely());
            extraComForSearch.setLayout(new GridLayout(3, false));
            fillExtraComForSearch(extraComForSearch);
        }

        if (mode == "edit"){
            fillInputs();
        }
        fillSubFields();
    }

    private void fillLeftCom(Composite leftCom, boolean useRequiredMark) {

        Label numberLabel = new Label(leftCom, SWT.NONE);
        if (useRequiredMark) numberLabel.setText("Nummer*: "); else numberLabel.setText("Nummer: ");
        numberInput = new Text(leftCom, SWT.BORDER);
        numberInput.setLayoutData(new CustomLayoutData().fillHorizontal());

        Label nameLabel = new Label(leftCom, SWT.NONE);
        if (useRequiredMark) nameLabel.setText("Name*: "); else numberLabel.setText("Name: ");
        nameInput = new Text(leftCom, SWT.BORDER);
        nameInput.setLayoutData(new CustomLayoutData().fillHorizontal());

        Label capacityLabel = new Label(leftCom, SWT.NONE);
        capacityLabel.setText("Kapazität: ");
        capacityInput = new Spinner(leftCom, SWT.BORDER);

        Label availableLabel = new Label(leftCom, SWT.NONE);
        availableLabel.setText("Verfügbar: ");
        availableButton = new Button(leftCom, SWT.CHECK);
        availableButton.setSelection(true);

        Label descriptionLabel = new Label(leftCom, SWT.NONE);
        if (useRequiredMark) descriptionLabel.setText("Beschreibung*: "); else descriptionLabel.setText("Beschreibung: ");
        descriptionLabel.setLayoutData(new CustomLayoutData().alignTop());
        descriptionInput = new Text(leftCom, SWT.BORDER | SWT.V_SCROLL | SWT.WRAP);
        descriptionInput.setLayoutData(new CustomLayoutData().fillCompletely());
    }

    private void fillRightCom(Composite rightCom, boolean useRequiredMark) {

        Label nameOfDepartmentLabel = new Label(rightCom, SWT.NONE);
        if (useRequiredMark) nameOfDepartmentLabel.setText("Niederlassung*: "); else nameOfDepartmentLabel.setText("Niederlassung: ");
        nameOfDepartmentInput = new Combo(rightCom, SWT.BORDER | SWT.DROP_DOWN | SWT.READ_ONLY);
        fillDepartmentCombo();
        nameOfDepartmentInput.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent e) {
                fillSubFields();
            }
        });
        nameOfDepartmentInput.addFocusListener(new FocusAdapter() {
            @Override
            public void focusGained(FocusEvent e) {
                fillDepartmentCombo();
            }
        });
        nameOfDepartmentInput.setLayoutData(new CustomLayoutData().fillHorizontal());

        Label streetWithNumberLabel = new Label(rightCom, SWT.NONE);
        streetWithNumberLabel.setText("Straße: ");
        streetWithNumberInput = new Text(rightCom, SWT.BORDER);
        streetWithNumberInput.setEditable(false);
        streetWithNumberInput.setLayoutData(new CustomLayoutData().fillHorizontal());

        Label plzNumberLabel = new Label(rightCom, SWT.NONE);
        plzNumberLabel.setText("Postleitzahl: ");
        plzNumberInput = new Text(rightCom, SWT.BORDER);
        plzNumberInput.setEditable(false);
        plzNumberInput.setLayoutData(new CustomLayoutData().fillHorizontal());

        Label locationLabel = new Label(rightCom, SWT.NONE);
        locationLabel.setText("Ort: ");
        locationInput = new Text(rightCom, SWT.BORDER);
        locationInput.setEditable(false);
        locationInput.setLayoutData(new CustomLayoutData().fillHorizontal());

        Label computerLabel = new Label(rightCom, SWT.NONE);
        computerLabel.setText("Computer: ");
        computerInput = new Spinner(rightCom, SWT.BORDER);

        Label beamerLabel = new Label(rightCom, SWT.NONE);
        beamerLabel.setText("Beamer: ");
        beamerInput = new Spinner(rightCom, SWT.BORDER);

        Label phoneLabel = new Label(rightCom, SWT.NONE);
        phoneLabel.setText("Telefone: ");
        phoneInput = new Spinner(rightCom, SWT.BORDER);

        Label seatLabel = new Label(rightCom, SWT.NONE);
        seatLabel.setText("Stühle: ");
        seatInput = new Spinner(rightCom, SWT.BORDER);

        Label tableLabel = new Label(rightCom, SWT.NONE);
        tableLabel.setText("Tische: ");
        tableInput = new Spinner(rightCom, SWT.BORDER);

        Label tvLabel = new Label(rightCom, SWT.NONE);
        tvLabel.setText("Fernseher: ");
        tvInput = new Spinner(rightCom, SWT.BORDER);
    }

    private void fillSubFields() {
        List<Niederlassung> branches = database.getAllBranches();
        if (branches.size() > 0) {
            Niederlassung branch = branches.get(nameOfDepartmentInput.getSelectionIndex());
            streetWithNumberInput.setText(branch.getStrasse() + " " + branch.getHausnummer());
            plzNumberInput.setText(branch.getPlz());
            locationInput.setText(branch.getOrt());
        }
    }

    private void fillDepartmentCombo() {

        nameOfDepartmentInput.removeAll();

        List<Niederlassung> branches = database.getAllBranches();
        for (int i = 0; i < branches.size(); i++){

            nameOfDepartmentInput.add(branches.get(i).getName());
        }
        nameOfDepartmentInput.select(0);
    }

    private void fillExtraComForSearch(Composite composite) {
        Label employeeLabel = new Label(composite, SWT.NONE);
        employeeLabel.setText("Mitarbeiter: ");
        employeeInput = new Text(composite, SWT.BORDER);
        employeeInput.setLayoutData(new CustomLayoutData().fillHorizontalSpan(2));

        Label dateOfBookingBeginningLabel = new Label(composite, SWT.NONE);
        dateOfBookingBeginningLabel.setText("Buchungs Anfang: ");
        dateOfBookingBeginningInput = new DateTime(composite, SWT.NONE);
        dateOfBookingBeginningInput.setLayoutData(new CustomLayoutData().fillHorizontal());
        dateOfBookingBeginningInputTime = new DateTime(composite, SWT.TIME);
        dateOfBookingBeginningInputTime.setLayoutData(new CustomLayoutData().fillHorizontal());

        Label dateOfBookingEndLabel = new Label(composite, SWT.NONE);
        dateOfBookingEndLabel.setText("Buchungs Ende: ");
        dateOfBookingEndInput = new DateTime(composite, SWT.NONE);
        dateOfBookingEndInput.setLayoutData(new CustomLayoutData().fillHorizontal());
        dateOfBookingEndInputTime = new DateTime(composite, SWT.TIME);
        dateOfBookingEndInputTime.setLayoutData(new CustomLayoutData().fillHorizontal());
    }

    private void fillInputs() {

        List<Niederlassung> branches = database.getAllBranches();

        Niederlassung branch = null;
        int index = 0;
        for (int x = 0; x < branches.size(); x++){
            long roomId = raum.getNiederlassung().getId();
            Long branchId = branches.get(x).getId();
            if (roomId == branchId){
                branch = branches.get(x);
                index = x;
            }
        }
        Ausstattung assets = database.getAssets(raum);

        numberInput.setText(raum.getRaumnummer());
        nameInput.setText(raum.getName());
        capacityInput.setSelection(raum.getKapazitaet());
        availableButton.setSelection(raum.isVerfuegbarkeit());
        descriptionInput.setText(raum.getBeschreibung());

        nameOfDepartmentInput.select(index);
        fillSubFields();

        if (assets != null) {
            computerInput.setSelection(assets.getComputer());
            beamerInput.setSelection(assets.getBeamer());
            phoneInput.setSelection(assets.getTelefone());
            seatInput.setSelection(assets.getStuehle());
            tableInput.setSelection(assets.getTische());
            tvInput.setSelection(assets.getFernseher());
        }
    }

    public void addButtons() {

        Composite buttonComposite = new Composite(this.parent, SWT.NONE);
        buttonComposite.setLayoutData(new CustomLayoutData().fillHorizontalWithButtonHeight(3));
        GridLayout buttonComLayout = new GridLayout(3, false);
        buttonComLayout.marginHeight = 0;
        buttonComLayout.marginWidth = 0;
        buttonComposite.setLayout(buttonComLayout);

        Composite buttonFiller =new Composite(buttonComposite, SWT.NONE);
        buttonFiller.setLayoutData(new CustomLayoutData().horizontalFiller());
        Button clearFieldButton = new Button(buttonComposite, SWT.NONE);
        clearFieldButton.setText("Felder leeren");
        clearFieldButton.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent e) {
                clearFields();
            }
        });
        clearFieldButton.setLayoutData(new CustomLayoutData().alignButtonRight());
        if (mode == "create"){
            Button createRoomButton = new Button(buttonComposite, SWT.NONE);
            createRoomButton.setText("Erstellen");
            createRoomButton.addSelectionListener(new SelectionAdapter() {
                @Override
                public void widgetSelected(SelectionEvent e) {
                    createRoomWithAssets();
                    roomList.fillList();
                }
            });
            createRoomButton.setLayoutData(new CustomLayoutData().alignButtonRight());
        } else{
            Button editRoomButton = new Button(buttonComposite, SWT.NONE);
            editRoomButton.setText("Ok");
            editRoomButton.addSelectionListener(new SelectionAdapter() {
                @Override
                public void widgetSelected(SelectionEvent e) {
                    editRoomWithAssets();
                }
            });
            editRoomButton.setLayoutData(new CustomLayoutData().alignButtonRight());
        }
    }

    public void addSearchButtons(){
        Composite buttonComposite = new Composite(this.parent, SWT.NONE);
        buttonComposite.setLayoutData(new CustomLayoutData().fillHorizontalWithButtonHeight(3));
        GridLayout buttonComLayout = new GridLayout(3, false);
        buttonComLayout.marginHeight = 0;
        buttonComLayout.marginWidth = 0;
        buttonComposite.setLayout(buttonComLayout);

        Composite buttonFiller =new Composite(buttonComposite, SWT.NONE);
        buttonFiller.setLayoutData(new CustomLayoutData().horizontalFiller());
        Button clearFieldButton = new Button(buttonComposite, SWT.NONE);
        clearFieldButton.setText("Felder leeren");
        clearFieldButton.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent e) {
                clearFields();
            }
        });
        clearFieldButton.setLayoutData(new CustomLayoutData().alignButtonRight());
        Button searchButton = new Button(buttonComposite, SWT.NONE);
        searchButton.setText("Suche");
        searchButton.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent e) {
                searchWithInputFields();
            }
        });
        searchButton.setLayoutData(new CustomLayoutData().alignButtonRight());
    }

    private void createRoomWithAssets() {
        Niederlassung branchSelected = getRightBranchFromList(nameOfDepartmentInput.getSelectionIndex());
        if (nameInput.getText().replaceAll("\\s","").length() > 0 &&
                descriptionInput.getText().replaceAll("\\s","").length() > 0 &&
                numberInput.getText().replaceAll("\\s","").length() > 0 &&
                branchSelected != null) {
            Raum toCreate = new Raum(nameInput.getText(), capacityInput.getSelection(), descriptionInput.getText(), numberInput.getText(), availableButton.getSelection(), branchSelected);
            database.createRoom(toCreate);
            Ausstattung assets = new Ausstattung(computerInput.getSelection(), phoneInput.getSelection(), seatInput.getSelection(), tableInput.getSelection(), tvInput.getSelection(), beamerInput.getSelection(), toCreate);
            database.createAssets(assets);
            clearFields();
        } else {
            new ErrorBox(parent.getShell(), "Hinweis", "Bitte füllen Sie alle benötigten Felder aus.");
        }
    }

    private Niederlassung getRightBranchFromList(int selectionIndex) {
        List<Niederlassung> branches = database.getAllBranches();
        return (selectionIndex >= 0 && branches.size() > 0) ? branches.get(selectionIndex) : null;
    }

    private void editRoomWithAssets() {
        Niederlassung branchSelected = getRightBranchFromList(nameOfDepartmentInput.getSelectionIndex());
        if (nameInput.getText().replaceAll("\\s","").length() > 0 &&
                descriptionInput.getText().replaceAll("\\s","").length() > 0 &&
                numberInput.getText().replaceAll("\\s","").length() > 0 &&
                branchSelected != null) {
            Raum toCreate = new Raum(nameInput.getText(), capacityInput.getSelection(), descriptionInput.getText(), numberInput.getText(), availableButton.getSelection(), branchSelected);
            toCreate.setId(raum.getId());
            database.editRoom(toCreate);
            Ausstattung assets = database.getAssets(raum);
            Ausstattung toEdit = new Ausstattung(computerInput.getSelection(), phoneInput.getSelection(), seatInput.getSelection(), tableInput.getSelection(), tvInput.getSelection(), beamerInput.getSelection(), raum);
            toEdit.setId(assets.getId());
            database.editAssets(toEdit);
            parent.getShell().close();
        } else {
            new ErrorBox(parent.getShell(), "Hinweis", "Bitte füllen Sie alle benötigten Felder aus.");
        }
    }

    private void searchWithInputFields() {
        roomList.fillListWithFilter(nameInput.getText(), capacityInput.getSelection(), numberInput.getText(), availableButton.getSelection(), nameOfDepartmentInput.getText(), "", "", "", "", computerInput.getSelection(), phoneInput.getSelection(), seatInput.getSelection(), tableInput.getSelection(), beamerInput.getSelection(), tvInput.getSelection());
    }

    private void clearFields() {
        numberInput.setText("");
        nameInput.setText("");
        capacityInput.setSelection(0);
        availableButton.setSelection(true);
        descriptionInput.setText("");

        computerInput.setSelection(0);
        beamerInput.setSelection(0);
        phoneInput.setSelection(0);
        seatInput.setSelection(0);
        tableInput.setSelection(0);
        tvInput.setSelection(0);

        if (mode == "search"){
            employeeInput.setText("");
            dateOfBookingBeginningInput.setDate(LocalDateTime.now().getYear(), LocalDateTime.now().getMonthValue(), LocalDateTime.now().getDayOfMonth());
            dateOfBookingBeginningInputTime.setTime(LocalDateTime.now().getHour(), LocalDateTime.now().getMinute(), LocalDateTime.now().getSecond());
            dateOfBookingEndInput.setDate(LocalDateTime.now().getYear(), LocalDateTime.now().getMonthValue(), LocalDateTime.now().getDayOfMonth());
            dateOfBookingEndInputTime.setTime(LocalDateTime.now().getHour(), LocalDateTime.now().getMinute(), LocalDateTime.now().getSecond());
        }
    }

    public void setList(RoomList list) {
        this.roomList = list;
    }
}

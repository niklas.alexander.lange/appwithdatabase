package database;

import database.entities.Ausstattung;
import database.entities.Buchungstermin;
import database.entities.Niederlassung;
import database.entities.Raum;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.query.Query;

import javax.persistence.NoResultException;
import java.sql.Timestamp;
import java.util.*;

public class DatabaseController implements IDatabaseController {

    // **** Session Factory ****
    private static SessionFactory sessionFactory;

    public void start() {
        final StandardServiceRegistry registry = new StandardServiceRegistryBuilder()
                .configure()
                .build();
        try {
            sessionFactory = new MetadataSources(registry).buildMetadata().buildSessionFactory();
        } catch (Exception ex) {
            StandardServiceRegistryBuilder.destroy(registry);
        }
    }

    protected void shutdown() {
        sessionFactory.close();
    }

    // **** Utilities ****

    private void saveToDatabase(Object obj) {
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        session.saveOrUpdate(obj);
        transaction.commit();
        session.close();
    }

    private void deleteFromDatabase(Object obj) {
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        session.delete(obj);
        transaction.commit();
        session.close();
    }

    // **** Raum ****

    @Override
    public void createRoom(Raum room) {
        saveToDatabase(room);
    }

    @Override
    public void deleteRoom(Raum room) {
        deleteFromDatabase(room);
    }

    @Override
    public void editRoom(Raum room) {
        saveToDatabase(room);
    }

    @Override
    public List<Raum> getAllRooms() {
        Session session = sessionFactory.openSession();
        String hql = "FROM Raum";
        Query query = session.createQuery(hql);
        List<Raum> result = query.getResultList();
        session.close();
        return result;
    }

    @Override
    public boolean canRoomBeBooked(Raum room, Timestamp startTime, Timestamp endTime) {
        Session session = sessionFactory.openSession();
        String hql = "FROM Buchungstermin B WHERE (B.raum.id = :r_id AND ((B.start >= :startTime AND B.start < :endTime) OR (B.ende > :startTime AND B.ende <= :endTime)))";
        Query query = session.createQuery(hql);
        query.setParameter("r_id", room.getId());
        query.setParameter("startTime", startTime);
        query.setParameter("endTime", endTime);
        List<Buchungstermin> result = query.getResultList();
        session.close();
        return result.size() == 0;
    }

    @Override
    public List<Raum> getFilteredRooms(String roomName, int capacity, String roomNumber, boolean available, String departmentName, String departmentStreet, String departmentStreetNumber, String departmentPlz, String departmentLocation, int computer, int phones, int seats, int tables, int beamer, int tvs) {
        // Filter Booleans
        String filterAvailable = available ? "Y" : "N";
        // Filter Integers
        HashMap<String, Object> intFilter = new HashMap<>();
        intFilter.put("capacity", capacity);
        intFilter.put("computer", computer);
        intFilter.put("phones", phones);
        intFilter.put("seats", seats);
        intFilter.put("tables", tables);
        intFilter.put("beamer", beamer);
        intFilter.put("tvs", tvs);
        // Filter Strings
        HashMap<String, String> strFilter = new HashMap<>();
        strFilter.put("roomName", roomName);
        strFilter.put("roomNumber", roomNumber);
        strFilter.put("departmentName", departmentName);
        strFilter.put("departmentStreet", departmentStreet);
        strFilter.put("departmentStreetNumber", departmentStreetNumber);
        strFilter.put("departmentPlz", departmentPlz);
        strFilter.put("departmentLocation", departmentLocation);
        strFilter.forEach((k, v) -> {if (v == null) strFilter.replace(k, "");});
        // Query
        Session session = sessionFactory.openSession();
        String hql = "SELECT A.raum FROM Ausstattung A WHERE A.raum.name LIKE :roomName AND A.raum.kapazitaet >= :capacity " +
                "AND A.raum.raumnummer LIKE :roomNumber AND A.raum.verfuegbarkeit = :available " +
                "AND A.raum.niederlassung.name LIKE :departmentName AND A.raum.niederlassung.strasse LIKE :departmentStreet " +
                "AND A.raum.niederlassung.hausnummer LIKE :departmentStreetNumber AND A.raum.niederlassung.plz LIKE :departmentPlz " +
                "AND A.raum.niederlassung.ort LIKE :departmentLocation AND A.computer >= :computer AND A.telefone >= :phones " +
                "AND A.stuehle >= :seats AND A.tische >= :tables AND A.beamer >= :beamer AND A.fernseher >= :tvs";
        Query query = session.createQuery(hql);
        query.setParameter("available", filterAvailable);
        intFilter.forEach(query::setParameter);
        strFilter.forEach((k, v) -> query.setParameter(k, "%" + v + "%"));
        List<Raum> result = query.getResultList();
        session.close();
        return result;
    }

    // **** Niederlassung ****

    @Override
    public void createBranch(Niederlassung branch) {
        saveToDatabase(branch);
    }

    @Override
    public void deleteBranch(Niederlassung branch) {
        deleteFromDatabase(branch);
    }

    @Override
    public void editBranch(Niederlassung branch) {
        saveToDatabase(branch);
    }

    @Override
    public List<Niederlassung> getAllBranches() {
        Session session = sessionFactory.openSession();
        String hql = "FROM Niederlassung";
        Query query = session.createQuery(hql);
        List<Niederlassung> result = query.getResultList();
        session.close();
        return result;
    }

    // **** Ausstattung ****

    @Override
    public void createAssets(Ausstattung assets) {
        saveToDatabase(assets);
    }

    @Override
    public void editAssets(Ausstattung assets) {
        saveToDatabase(assets);
    }

    @Override
    public Ausstattung getAssets(Raum raum) {
        try {
            Session session = sessionFactory.openSession();
            String hql = "FROM Ausstattung A WHERE A.raum.id = :r_id";
            Query query = session.createQuery(hql);
            query.setParameter("r_id", raum.getId());
            Ausstattung result = (Ausstattung) query.getSingleResult();
            session.close();
            return result;
        } catch (NoResultException e) {
            return null;
        }
    }

    // **** Buchungen ****

    @Override
    public void createBooking(Buchungstermin booking) {
        saveToDatabase(booking);
    }

    @Override
    public void deleteBooking(Buchungstermin booking) {
        deleteFromDatabase(booking);
    }

    @Override
    public void editBooking(Buchungstermin booking) {
        saveToDatabase(booking);
    }

    @Override
    public List<Buchungstermin> getBookings(Raum room) {
        Session session = sessionFactory.openSession();
        String hql = "FROM Buchungstermin B WHERE B.raum.id = :r_id";
        Query query = session.createQuery(hql);
        query.setParameter("r_id", room.getId());
        List<Buchungstermin> result = query.getResultList();
        session.close();
        return result;
    }
}

package database;

import database.entities.Ausstattung;
import database.entities.Buchungstermin;
import database.entities.Niederlassung;
import database.entities.Raum;

import java.sql.Timestamp;
import java.util.List;

public interface IDatabaseController {

    /**
     * ---Räume--------------------------------
     */

    public void createRoom(Raum room);

    public void deleteRoom(Raum room);

    public void editRoom(Raum room);

    public List<Raum> getAllRooms();

    public boolean canRoomBeBooked(Raum room, Timestamp startTime, Timestamp endTime);

    public List<Raum> getFilteredRooms(String roomName, int capacity, String roomNumber, boolean available,
                                       String departmentName, String departmentStreet, String departmentStreetNumber, String departmentPlz, String departmentLocation,
                                       int computer, int phones, int seats, int tables, int beamer, int tvs);

    /**
     * ---Niederlassungen-----------------------
     */

    public void createBranch(Niederlassung branch);

    public void deleteBranch(Niederlassung branch);

    public void editBranch(Niederlassung branch);

    public List<Niederlassung> getAllBranches();

    /**
     * ---Austattung----------------------------
     */

    public void createAssets(Ausstattung assets);

    public void editAssets(Ausstattung assets);

    public Ausstattung getAssets(Raum raum);

    /**
     * ---Buchungstermin-------------------------
     */

    public void createBooking(Buchungstermin booking);

    public void deleteBooking(Buchungstermin booking);

    public void editBooking(Buchungstermin booking);

    public List<Buchungstermin> getBookings(Raum room);


}

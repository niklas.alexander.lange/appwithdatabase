package database;

import database.entities.Ausstattung;
import database.entities.Buchungstermin;
import database.entities.Niederlassung;
import database.entities.Raum;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

public class TestDatenFactory {

    public static void main(String[] args) {
        DatabaseController dc = new DatabaseController();
        dc.start();
        System.out.println(" ");
        System.out.println(" ");
        System.out.println(" ");

        // Testdaten
        System.out.println("--- Erstelle Testdaten ---");
        Niederlassung n1 = new Niederlassung("PLATH GmbH", "Gotenstraße", "18", "20097", "Hamburg");
        Niederlassung n2 = new Niederlassung("RARI Food Int. GmbH", "Hovestraße", "72", "20539", "Hamburg");
        Niederlassung n3 = new Niederlassung("Feddersen Gastro GmbH", "Beusselstraße", "44 n-q", "10553", "Berlin");
        Niederlassung n4 = new Niederlassung("IT-Consulting", "Werner-Heisenberg-Allee", "25", "80939", "München");
        Niederlassung n5 = new Niederlassung("Altmarkt-Büro", "Altmarkt", "10 B/D", "01067", "Dresden");

        Raum r1 = new Raum("Besprechungsraum-Groß", 30, "Für größere Besprechungen", "E217", true, n2);
        Raum r2 = new Raum("Besprechungsraum-Klein", 10, "Für kleine Meetings", "E219", true, n2);
        Raum r3 = new Raum("Halle", 100, "Eine große Halle", "A20", false, n1);
        Raum r4 = new Raum("Altmarkt Saal", 56, "Ein Saal für größere Veranstaltungen", "Saal 1", true, n5);
        Raum r5 = new Raum("IT-Besprechungsraum", 6, "Besprechungsraum", "", true, n4);
        Raum r6 = new Raum("Glas-Besprechungsraum", 12, "Besprechungsraum Glaskasten", "R-1", true, n3);
        Raum r7 = new Raum("Fitnessraum", 25, "Sport", "A76", true, n1);
        Raum r8 = new Raum("Entspannungsraum", 4, "Zur Entspannung", "", false, n5);

        Ausstattung a1 = new Ausstattung(6, 2, 30,15, 0, 1, r1);
        Ausstattung a2 = new Ausstattung(0, 1, 10,4, 1, 0, r2);
        Ausstattung a3 = new Ausstattung(0, 4, 80,25, 0, 1, r3);
        Ausstattung a4 = new Ausstattung(0, 0, 50,17, 2, 0, r4);
        Ausstattung a5 = new Ausstattung(1, 1, 6,2, 1, 0, r5);
        Ausstattung a6 = new Ausstattung(0, 0, 12,7, 0, 0, r6);
        Ausstattung a7 = new Ausstattung(0, 0, 0,0, 4, 0, r7);
        Ausstattung a8 = new Ausstattung(0, 0, 4,0, 1, 1, r8);

        System.out.println(System.currentTimeMillis());
        long time = 1590617029;

//        Buchungstermin b1 = new Buchungstermin("Tobias Henn", new Timestamp(System.currentTimeMillis()), new Timestamp(1590660000*1000000), new Timestamp(1590541726*1000000), r1);
//        Buchungstermin b2 = new Buchungstermin("Tim Endert", new Timestamp(1590822900*1000000), new Timestamp(1590940800*1000000), new Timestamp(1590563794*1000000), r2);
//        Buchungstermin b3 = new Buchungstermin("Max Mustermann", new Timestamp(1590678000), new Timestamp(1590701400), new Timestamp(1590573778), r1);
//        Buchungstermin b4 = new Buchungstermin("Jan Ruprecht", new Timestamp(1603774800), new Timestamp(1603809000), new Timestamp(1590576790), r4);
//        Buchungstermin b5 = new Buchungstermin("Mhbob Alhoussen", new Timestamp(1593855000), new Timestamp(1594360800), new Timestamp(1590615717), r6);
//        Buchungstermin b6 = new Buchungstermin("Herr Meier", new Timestamp(1604056500), new Timestamp(1604134800), new Timestamp(1590627845), r5);
//        Buchungstermin b7 = new Buchungstermin("Jasmin Recht", new Timestamp(1604056500), new Timestamp(1604156400), new Timestamp(1590646345), r7);
//        Buchungstermin b8 = new Buchungstermin("Lara Siemer", new Timestamp(1590942600), new Timestamp(1590957000), new Timestamp(1590664017), r2);
//        Buchungstermin b9 = new Buchungstermin("Klaus Knecht", new Timestamp(1596356100), new Timestamp(1596358800), new Timestamp(1590676635), r4);
//        Buchungstermin b10 = new Buchungstermin("Luis Leiter", new Timestamp(1633853400), new Timestamp(1633893660), new Timestamp(1590697245), r6);

        dc.createBranch(n1);
        dc.createBranch(n2);
        dc.createBranch(n3);
        dc.createBranch(n4);
        dc.createBranch(n5);

        dc.createRoom(r1);
        dc.createRoom(r2);
        dc.createRoom(r3);
        dc.createRoom(r4);
        dc.createRoom(r5);
        dc.createRoom(r6);
        dc.createRoom(r7);
        dc.createRoom(r8);

        dc.createAssets(a1);
        dc.createAssets(a2);
        dc.createAssets(a3);
        dc.createAssets(a4);
        dc.createAssets(a5);
        dc.createAssets(a6);
        dc.createAssets(a7);
        dc.createAssets(a8);

//        dc.createBooking(b1);
//        dc.createBooking(b2);
//        dc.createBooking(b3);
//        dc.createBooking(b4);
//        dc.createBooking(b5);
//        dc.createBooking(b6);
//        dc.createBooking(b7);
//        dc.createBooking(b8);
//        dc.createBooking(b9);
//        dc.createBooking(b10);

        System.out.println("> Niederlassungen:");
        List<Niederlassung> listN = dc.getAllBranches();
        for (Niederlassung n : listN) System.out.println(n.toString());

        System.out.println("> Räume:");
        List<Raum> listR = dc.getAllRooms();
        for (Raum r : listR) System.out.println(r.toString());

        System.out.println("--- Filtertest ---");
        List<Raum> filteredRooms = dc.getFilteredRooms("", 10, "", false, "", "", "", "", "", 0, 0,0,0,0,0);
        for (Raum r : filteredRooms) System.out.println(r.toString());

        System.out.println(" ");
        System.out.println(" ");
        System.out.println(" ");
        dc.shutdown();
    }
}

package database.entities;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "ausstattung")
public class Ausstattung implements Serializable {

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "id")
    private long id;
    @Column(name = "computer")
    private int computer;
    @Column(name = "telefone")
    private int telefone;
    @Column(name = "stuehle")
    private int stuehle;
    @Column(name = "tische")
    private int tische;
    @Column(name = "fernseher")
    private int fernseher;
    @Column(name = "beamer")
    private int beamer;
    @OneToOne
    @JoinColumn(name = "raum_id", referencedColumnName = "id")
    @OnDelete(action = OnDeleteAction.CASCADE)
    private Raum raum;

    public Ausstattung() {}

    public Ausstattung(int computer, int telefone, int stuehle, int tische, int fernseher, int beamer, Raum raum) {
        this.computer = computer;
        this.telefone = telefone;
        this.stuehle = stuehle;
        this.tische = tische;
        this.fernseher = fernseher;
        this.beamer = beamer;
        this.raum = raum;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getComputer() {
        return computer;
    }

    public void setComputer(int computer) {
        this.computer = computer;
    }

    public int getTelefone() {
        return telefone;
    }

    public void setTelefone(int telefone) {
        this.telefone = telefone;
    }

    public int getStuehle() {
        return stuehle;
    }

    public void setStuehle(int stuehle) {
        this.stuehle = stuehle;
    }

    public int getTische() {
        return tische;
    }

    public void setTische(int tische) {
        this.tische = tische;
    }

    public int getFernseher() {
        return fernseher;
    }

    public void setFernseher(int fernseher) {
        this.fernseher = fernseher;
    }

    public int getBeamer() {
        return beamer;
    }

    public void setBeamer(int beamer) {
        this.beamer = beamer;
    }

    public Raum getRaum() {
        return raum;
    }

    public void setRaum(Raum raum) {
        this.raum = raum;
    }

    @Override
    public String toString() {
        return "Ausstattung{" +
                "id=" + id +
                ", computer=" + computer +
                ", telefone=" + telefone +
                ", stuehle=" + stuehle +
                ", tische=" + tische +
                ", fernseher=" + fernseher +
                ", beamer=" + beamer +
                ", raum=" + raum +
                '}';
    }

}

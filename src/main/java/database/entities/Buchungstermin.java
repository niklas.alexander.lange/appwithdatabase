package database.entities;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;
import java.time.LocalDateTime;

@Entity
@Table(name = "buchungstermin")
public class Buchungstermin implements Serializable {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column(name = "id")
    private long id;
    @Column(name = "mitarbeiter")
    private String mitarbeiter;
    @Column(name = "start")
    private Timestamp start;
    @Column(name = "ende")
    private Timestamp ende;
    @Column(name = "erstellung")
    private Timestamp erstellung;
    @ManyToOne
    @JoinColumn(name = "raum_id", referencedColumnName = "id")
    @OnDelete(action = OnDeleteAction.CASCADE)
    private Raum raum;

    public Buchungstermin() {}

    public Buchungstermin(String mitarbeiter, Timestamp start, Timestamp ende, Timestamp erstellung, Raum raum) {
        this.mitarbeiter = mitarbeiter;
        this.start = start;
        this.ende = ende;
        this.erstellung = erstellung;
        this.raum = raum;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getMitarbeiter() {
        return mitarbeiter;
    }

    public void setMitarbeiter(String mitarbeiter) {
        this.mitarbeiter = mitarbeiter;
    }

    public Timestamp getStart() {
        return start;
    }

    public void setStart(Timestamp start) {
        this.start = start;
    }

    public Timestamp getEnde() {
        return ende;
    }

    public void setEnde(Timestamp ende) {
        this.ende = ende;
    }

    public Timestamp getErstellung() {
        return erstellung;
    }

    public void setErstellung(Timestamp erstellung) {
        this.erstellung = erstellung;
    }

    public Raum getRaum() {
        return raum;
    }

    public void setRaum(Raum raum) {
        this.raum = raum;
    }

    @Override
    public String toString() {
        return "Buchungstermin{" +
                "id=" + id +
                ", mitarbeiter='" + mitarbeiter + '\'' +
                ", start=" + start +
                ", ende=" + ende +
                ", erstellung=" + erstellung +
                ", raum=" + raum +
                '}';
    }

}

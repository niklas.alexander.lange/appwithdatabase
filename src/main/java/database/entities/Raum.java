package database.entities;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;

@Entity
@Table(name = "raum")
public class Raum implements Serializable {

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "id")
    private long id;
    @Column(name = "name")
    private String name;
    @Column(name = "kapazitaet")
    private int kapazitaet;
    @Column(name = "beschreibung")
    private String beschreibung;
    @Column(name = "raumnummer")
    private String raumnummer;
    @Column(name = "verfuegbarkeit")
    @Type(type="yes_no")
    private boolean verfuegbarkeit;
    @ManyToOne
    @JoinColumn(name = "niederlassung_id", referencedColumnName = "id")
    @OnDelete(action = OnDeleteAction.CASCADE)
    private Niederlassung niederlassung;

    public Raum() {}

    public Raum(String name, int kapazitaet, String beschreibung, String raumnummer, boolean verfuegbarkeit, Niederlassung niederlassung) {
        this.name = name;
        this.kapazitaet = kapazitaet;
        this.beschreibung = beschreibung;
        this.raumnummer = raumnummer;
        this.verfuegbarkeit = verfuegbarkeit;
        this.niederlassung = niederlassung;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getKapazitaet() {
        return kapazitaet;
    }

    public void setKapazitaet(int kapazitaet) {
        this.kapazitaet = kapazitaet;
    }

    public String getBeschreibung() {
        return beschreibung;
    }

    public void setBeschreibung(String beschreibung) {
        this.beschreibung = beschreibung;
    }

    public String getRaumnummer() {
        return raumnummer;
    }

    public void setRaumnummer(String raumnummer) {
        this.raumnummer = raumnummer;
    }

    public boolean isVerfuegbarkeit() {
        return verfuegbarkeit;
    }

    public void setVerfuegbarkeit(boolean verfuegbarkeit) {
        this.verfuegbarkeit = verfuegbarkeit;
    }

    public Niederlassung getNiederlassung() {
        return niederlassung;
    }

    public void setNiederlassung(Niederlassung niederlassung) {
        this.niederlassung = niederlassung;
    }

    @Override
    public String toString() {
        return "Raum{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", kapazitaet=" + kapazitaet +
                ", beschreibung='" + beschreibung + '\'' +
                ", raumnummer='" + raumnummer + '\'' +
                ", verfuegbarkeit=" + verfuegbarkeit +
                ", niederlassung=" + niederlassung +
                '}';
    }

}

package database.entities;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "niederlassung")
public class Niederlassung implements Serializable {

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "id")
    private long id;
    @Column(name = "name")
    private String name;
    @Column(name = "strasse")
    private String strasse;
    @Column(name = "hausnummer")
    private String hausnummer;
    @Column(name = "plz")
    private String plz;
    @Column(name = "ort")
    private String ort;

    public Niederlassung() {}

    public Niederlassung(String name, String strasse, String hausnummer, String plz, String ort) {
        this.name = name;
        this.strasse = strasse;
        this.hausnummer = hausnummer;
        this.plz = plz;
        this.ort = ort;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStrasse() {
        return strasse;
    }

    public void setStrasse(String strasse) {
        this.strasse = strasse;
    }

    public String getHausnummer() {
        return hausnummer;
    }

    public void setHausnummer(String hausnummer) {
        this.hausnummer = hausnummer;
    }

    public String getPlz() {
        return plz;
    }

    public void setPlz(String plz) {
        this.plz = plz;
    }

    public String getOrt() {
        return ort;
    }

    public void setOrt(String ort) {
        this.ort = ort;
    }

    @Override
    public String toString() {
        return "Niederlassung{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", strasse='" + strasse + '\'' +
                ", hausnummer='" + hausnummer + '\'' +
                ", plz='" + plz + '\'' +
                ", ort=" + ort +
                '}';
    }

}

package tabs;

import layout.CustomLayoutData;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.TabFolder;
import widgets.BranchCreationEdit;
import widgets.BranchList;

public class BranchManagementComposite extends Composite {

    BranchCreationEdit creation;
    BranchList list;

    public BranchManagementComposite(TabFolder parent, int style) {
        super(parent, style);

        GridLayout compositeLay = new GridLayout();
        compositeLay.numColumns = 1;

        this.setLayout(compositeLay);
        createRoomGroup();
        deleteAndEditRoomGroup();
        this.pack();
    }

    private void createRoomGroup() {

        Group createGroup = new Group(this, SWT.NONE);
        createGroup.setText("Niederlassung erstellen");
        createGroup.setLayoutData(new CustomLayoutData().fillHorizontal());
        GridLayout layout = new GridLayout(1, true);
        layout.marginHeight = 0;
        layout.marginWidth = 0;
        createGroup.setLayout(layout);

        creation = new BranchCreationEdit(createGroup, "create", null);
        creation.addContent();
        creation.addButtons();

        createGroup.pack();
    }

    private void deleteAndEditRoomGroup() {

        Group editGroup = new Group(this, SWT.NONE);
        editGroup.setText("Niederlassung bearbeiten");
        editGroup.setLayoutData(new CustomLayoutData().fillCompletely());
        GridLayout layout = new GridLayout(1, true);
        layout.marginHeight = 0;
        layout.marginWidth = 0;
        editGroup.setLayout(layout);


        list = new BranchList(editGroup);
        list.addContent();
        list.addButtons();
        creation.setBranchList(list);

        editGroup.pack();
    }

    public void updateList(){
        list.fillList();
    }
}

package tabs;

import layout.CustomLayoutData;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.TabFolder;
import widgets.RoomCreationEditDelete;
import widgets.RoomList;

public class RoomManagementComposite extends Composite {

    RoomCreationEditDelete creation;

    public RoomManagementComposite(TabFolder parent, int style) {
        super(parent, style);

        GridLayout compositeLay = new GridLayout();
        compositeLay.numColumns = 1;

        this.setLayout(compositeLay);
        createRoomGroup();
        deleteAndEditRoomGroup();
        this.pack();
    }

    private void createRoomGroup() {
        Group createGroup = new Group(this, SWT.NONE);
        createGroup.setText("Raum erstellen");
        createGroup.setLayoutData(new CustomLayoutData().fillHorizontal());
        GridLayout layout = new GridLayout(2, true);
        layout.marginHeight = 0;
        layout.marginWidth = 0;
        createGroup.setLayout(layout);

        creation = new RoomCreationEditDelete(createGroup, "create", null, null);
        creation.addContent(true);
        creation.addButtons();

        createGroup.pack();
    }

    private void deleteAndEditRoomGroup() {
        Group editGroup = new Group(this, SWT.NONE);
        editGroup.setText("Raum bearbeiten");
        editGroup.setLayoutData(new CustomLayoutData().fillCompletely());
        GridLayout layout = new GridLayout(1, true);
        layout.marginHeight = 0;
        layout.marginWidth = 0;
        editGroup.setLayout(layout);

        RoomList list = new RoomList(editGroup);
        list.addContent();
        list.addEditDeleteButtons();
        creation.setList(list);

        editGroup.pack();
    }
}

package tabs;

import layout.CustomLayoutData;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.TabFolder;
import widgets.Booking;
import widgets.BookingList;
import widgets.RoomCreationEditDelete;
import widgets.RoomList;

public class RoomSearchCopmposite extends Composite {

    RoomList roomList;

    RoomCreationEditDelete search;

    BookingList list;

    public RoomSearchCopmposite(TabFolder parent, int style) {
        super(parent, style);

        GridLayout compositeLay = new GridLayout();
        compositeLay.numColumns = 2;

        this.setLayout(compositeLay);
        createSearchGroup();
        creatRoomListGroup();
        createBookingListGroup();
        this.pack();
    }

    private void createSearchGroup() {
        Group searchGroup = new Group(this, SWT.NONE);
        searchGroup.setText("Räume durchsuchen");
        searchGroup.setLayoutData(new CustomLayoutData().fillHorizontalSpan(2));
        GridLayout layout = new GridLayout(3, false);
        layout.marginHeight = 0;
        layout.marginWidth = 0;
        searchGroup.setLayout(layout);

        search = new RoomCreationEditDelete(searchGroup, "search", null, roomList);
        search.addContent(false);
        search.addSearchButtons();

        searchGroup.pack();
    }

    private void creatRoomListGroup() {
        Group listGroup = new Group(this, SWT.NONE);
        listGroup.setText("Raumliste");
        listGroup.setLayoutData(new CustomLayoutData().fillCompletely());
        GridLayout layout = new GridLayout(1, true);
        layout.marginHeight = 0;
        layout.marginWidth = 0;
        listGroup.setLayout(layout);

        roomList = new RoomList(listGroup);
        roomList.addContent();
        roomList.addBookingButtons();
        search.setList(roomList);
    }

    private void createBookingListGroup() {
        Group listGroup = new Group(this, SWT.NONE);
        listGroup.setText("Buchungen");
        listGroup.setLayoutData(new CustomLayoutData().bookingList());
        GridLayout layout = new GridLayout(1, true);
        layout.marginHeight = 0;
        layout.marginWidth = 0;
        listGroup.setLayout(layout);

        list = new BookingList(listGroup);
        roomList.setBookingList(list);
    }
}

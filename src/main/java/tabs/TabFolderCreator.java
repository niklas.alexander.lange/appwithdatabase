package tabs;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.TabFolder;
import org.eclipse.swt.widgets.TabItem;

/**
 * Creates all of the main Tabs
 */
public class TabFolderCreator {

    final Shell mainShell;
    final Rectangle mainClientArea;

    public TabFolderCreator(Shell mainShell, Rectangle mainClientArea) {
        this.mainShell = mainShell;
        this.mainClientArea = mainClientArea;
    }

    public TabFolder createMainTabs() {
        final TabFolder tabs = new TabFolder(this.mainShell, SWT.NONE);
        tabs.setLocation(this.mainClientArea.x, this.mainClientArea.y);
        TabItem roomSearchTab = new TabItem(tabs, SWT.NONE);
        roomSearchTab.setText("Raum buchen");
        roomSearchTab.setControl(new RoomSearchCopmposite(tabs, SWT.NONE));
        TabItem managementTab = new TabItem(tabs, SWT.NONE);
        managementTab.setText("Verwaltung");

        TabFolder subTabsManage = new TabFolder(tabs, SWT.NONE);
        managementTab.setControl(subTabsManage);
        TabItem roomCreationTab = new TabItem(subTabsManage, SWT.NONE);
        roomCreationTab.setText("Räume");
        roomCreationTab.setControl(new RoomManagementComposite(subTabsManage, SWT.NONE));
        TabItem branchCreationTab = new TabItem(subTabsManage, SWT.NONE);
        branchCreationTab.setText("Nierderlassungen");
        branchCreationTab.setControl(new BranchManagementComposite(subTabsManage, SWT.NONE));

        return tabs;
    }
}

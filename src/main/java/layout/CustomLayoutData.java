package layout;

import org.eclipse.swt.layout.GridData;

/**
 * Creates specified GridData
 */
public class CustomLayoutData {

    public GridData fillCompletely(){

        GridData data = new GridData();
        data.grabExcessHorizontalSpace = true;
        data.horizontalAlignment = GridData.FILL;
        data.grabExcessVerticalSpace = true;
        data.verticalAlignment = GridData.FILL;
        return data;
    }

    public GridData fillHorizontalWithButtonHeight(int span) {

        GridData data = new GridData();
        data.grabExcessHorizontalSpace = true;
        data.horizontalAlignment = GridData.FILL;
        data.heightHint = 30;
        data.horizontalSpan = span;
        return data;
    }

    public GridData alignButtonRight(){

        GridData data = new GridData();
        data.horizontalAlignment = GridData.END;
        return data;
    }

    public GridData horizontalFiller() {

        GridData data = new GridData();
        data.heightHint = 0;
        data.grabExcessHorizontalSpace = true;
        data.horizontalAlignment = GridData.FILL;
        return data;
    }

    public GridData fillHorizontal() {

        GridData data = new GridData();
        data.grabExcessHorizontalSpace = true;
        data.horizontalAlignment = GridData.FILL;
        return data;
    }

    public GridData alignCenter() {

        GridData data = new GridData();
        data.horizontalAlignment = GridData.CENTER;
        return data;
    }

    public GridData alignTop() {

        GridData data = new GridData();
        data.verticalIndent = 2;
        data.verticalAlignment = GridData.BEGINNING;
        return data;
    }

    public Object fillHorizontalSpan(int span) {
        GridData data = new GridData();
        data.grabExcessHorizontalSpace = true;
        data.horizontalAlignment = GridData.FILL;
        data.horizontalSpan = span;
        return data;
    }

    public Object bookingList() {

        GridData data = new GridData();
        data.grabExcessVerticalSpace = true;
        data.verticalAlignment = GridData.FILL;
        data.widthHint = 550;
        return data;
    }
}
